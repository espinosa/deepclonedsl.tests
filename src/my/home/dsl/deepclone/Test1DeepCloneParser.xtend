package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Body
import my.home.dsl.deepClone.ComplexField
import my.home.dsl.deepClone.Model
import my.home.dsl.deepClone.SimpleExcludedField
import my.home.dsl.deepClone.SimpleField
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*
import my.home.dsl.deepClone.ReferenceField

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneMainTest {
	
	@Inject ParseHelper<Model> parser
	@Inject ValidationTestHelper validationHelper
 
	 /** parse DSL referring existing class OpeningHours2 */
	@Test
	def void parseOpeningHours1() {
		val code = '''
		deepClone
		package a.b.c
		a.b.c.WeekdayOpeningHours OHCloner {
			weekDay
			hours
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(1, body.cloners.size)
		val classToClone = body.cloners.head.classToClone
		assertEquals("WeekdayOpeningHours", classToClone.simpleName)
		assertEquals("a.b.c.WeekdayOpeningHours", classToClone.qualifiedName)
		assertEquals("a.b.c.WeekdayOpeningHours", classToClone.identifier)
	}
	
	/** parse DSL referring nonexisting class OpeningHours2 */
	@Test(expected=typeof(AssertionError))
	def void parseOpeningHours2() {
		val code = '''
		deepClone
		package a.b.c
		my.home.dsl.deepclone.FooOpeningHours {
			weekDay 
		}'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void testNestedElement() {
		val code = '''
		deepClone
		package a.b.c
		a.b.c.WeekdayOpeningHours {
			weekDay
			hours {
				from
				to
			}
		}'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(1, body.cloners.size)
		assertEquals("a.b.c.WeekdayOpeningHours", body.cloners.head.classToClone.qualifiedName)
		assertEquals(2, body.cloners.head.fields.size)
		assertEquals("weekDay",  (body.cloners.head.fields.get(0) as SimpleField).fieldName)
		assertEquals("hours", (body.cloners.head.fields.get(1) as ComplexField).fieldName)
		assertTrue(body.cloners.head.fields.get(1) instanceof ComplexField)
		assertEquals("from", ((body.cloners.head.fields.get(1) as ComplexField).fields.get(0) as SimpleField).fieldName)
		assertEquals("to", ((body.cloners.head.fields.get(1) as ComplexField).fields.get(1) as SimpleField).fieldName)
	}
	
	@Test
	def void testNestedNestedElement() {
		val code = '''
		deepClone
		
		package my.home.cloners.reduced
		
		a.b.m.Magazine MagazineCloner123 {
			name
			-isbn
			article {
				name
				-author
				-pageNumber
				section {
					-name
					description
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(1, body.cloners.size)
		assertEquals("a.b.m.Magazine", body.cloners.head.classToClone.qualifiedName)
		assertEquals(3, body.cloners.head.fields.size)
		assertEquals("name",    (body.cloners.head.fields.get(0) as SimpleField).fieldName)
		assertEquals("isbn",    (body.cloners.head.fields.get(1) as SimpleExcludedField).fieldName)
		assertEquals("article", (body.cloners.head.fields.get(2) as ComplexField).fieldName)
		val article = body.cloners.head.fields.get(2) as ComplexField
		assertEquals("name",       (article.fields.get(0) as SimpleField).fieldName)
		assertEquals("author",     (article.fields.get(1) as SimpleExcludedField).fieldName)
		assertEquals("pageNumber", (article.fields.get(2) as SimpleExcludedField).fieldName)
		assertEquals("section",    (article.fields.get(3) as ComplexField).fieldName)
		val section = article.fields.get(3) as ComplexField
		assertEquals("name",        (section.fields.get(0) as SimpleExcludedField).fieldName)
		assertEquals("description", (section.fields.get(1) as SimpleField).fieldName)
		assertEquals("paragraph",   (section.fields.get(2) as SimpleExcludedField).fieldName)
	}

	@Test
	def void testSimpleExclusions() {
		val code = '''
		deepClone
		package a.b.c
		a.b.c.WeekdayOpeningHours {
			-weekDay
			hours
		}'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(1, body.cloners.size)
		assertEquals("a.b.c.WeekdayOpeningHours", body.cloners.head.classToClone.qualifiedName)
		assertEquals(2, body.cloners.head.fields.size)
		assertEquals("weekDay",  (body.cloners.head.fields.get(0) as SimpleExcludedField).fieldName)
		assertEquals("hours",     (body.cloners.head.fields.get(1) as SimpleField).fieldName)
	}
	
	@Test
	def void commensShouldBeSupportedButTransparent() {
		val code = '''
		deepClone
		package a.b.c
		// comment 1
		/* comment 2 */
		a.b.c.WeekdayOpeningHours {
			//comment 3
			weekDay  // comment 4
			/* 
			long
			multiline 
			comment 
			*/
			hours // comment 5
		}
		// comment 6
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(1, body.cloners.size)
		assertEquals("a.b.c.WeekdayOpeningHours", body.cloners.head.classToClone.qualifiedName)
		assertEquals(2, body.cloners.head.fields.size)
		assertEquals("weekDay",  (body.cloners.head.fields.get(0) as SimpleField).fieldName)
		assertEquals("hours",     (body.cloners.head.fields.get(1) as SimpleField).fieldName)
	}
	
	
	@Test
	def void complexExampleWithClonerReferences() {
		val code = '''
		deepClone
		
		package a.b.m
		
		a.b.m.Book {
			name
			-author
			&section SectionDeep
		}
		
		a.b.m.Section SectionDeep {
			name
			description
			paragraph {
				number
				text
			}
		}
		
		a.b.m.Section SectionReduced {
			name
			description
			-paragraph
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
		val body = model as Body
		assertEquals(3, body.cloners.size)
		assertEquals("a.b.m.Book",     body.cloners.get(0).classToClone.qualifiedName)
		assertEquals(null,             body.cloners.get(0).name)
		assertEquals("a.b.m.Section",  body.cloners.get(1).classToClone.qualifiedName)
		assertEquals("SectionDeep",    body.cloners.get(1).name)
		assertEquals("a.b.m.Section",  body.cloners.get(2).classToClone.qualifiedName)
		assertEquals("SectionReduced", body.cloners.get(2).name)
		
		val bookCloner = body.cloners.get(0)
		assertEquals(3, bookCloner.fields.size)
		assertEquals("name", (bookCloner.fields.get(0) as SimpleField).fieldName)
		assertEquals("author", (bookCloner.fields.get(1) as SimpleExcludedField).fieldName)
		assertEquals("section", (bookCloner.fields.get(2) as ReferenceField).fieldName)
		
		val clonerReferenceField = bookCloner.fields.get(2) as ReferenceField
		assertEquals("SectionDeep",   clonerReferenceField.clonerReference.name)
		assertEquals("a.b.m.Section", clonerReferenceField.clonerReference.classToClone.qualifiedName)
		assertEquals("description", ((bookCloner.fields.get(2) as ReferenceField).clonerReference.fields.get(1) as SimpleField).fieldName)
	}
}