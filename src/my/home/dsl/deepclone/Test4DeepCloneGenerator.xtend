package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Model
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.junit4.validation.ValidationTestHelper

/**
 * Parse sample DeepClone DSL file and test DeepClone to Java generator.  
 * Files are generated to memory, see {@link InMemoryFileSystemAccess}, perfect for testing.
 * @see http://christiandietrich.wordpress.com/2012/05/08/unittesting-xtend-generators/
 * 
 * @author espinosa
 */
@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DeepCloneInjectorProvider))
class DeepCloneGeneratorTest {
	
	@Inject IGenerator generatorDslToJava
	@Inject ParseHelper<Model> parseHelper
	@Inject ValidationTestHelper validationHelper
	
	@Test
	def testWithPackageName() {
		val code = '''
		deepClone 
		
		package my.home.cloners
		
		a.b.c.WeekdayOpeningHours {
			weekDay
			hours {
				from
				to
			}
		}'''
		val model = parseHelper.parse(code)
		validationHelper.assertNoErrors(model)
		
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.files.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "my/home/cloners/WeekdayOpeningHoursCloner.java"
		assertTrue(fsa.files.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package my.home.cloners;
			public class WeekdayOpeningHoursCloner {
				public a.b.c.WeekdayOpeningHours apply(a.b.c.WeekdayOpeningHours other) {
					a.b.c.WeekdayOpeningHours it = new a.b.c.WeekdayOpeningHours();
					it.setWeekDay(other.getWeekDay());
					it.setHours(hoursCloner.apply(other.getHours()));
					return it;
				}
				
				private final HoursCloner hoursCloner = new HoursCloner();
				public static class HoursCloner {
					public a.b.c.FromToItem apply(a.b.c.FromToItem other) {
						a.b.c.FromToItem it = new a.b.c.FromToItem();
						it.setFrom(other.getFrom());
						it.setTo(other.getTo());
						return it;
					}
				}
			}
			'''.toString,
			fsa.files.get(topClonerJavaFileName).toString
		)
	}
	
	@Test
	def testWithoutPackageName() {
		val code = '''
		deepClone	
		a.b.c.WeekdayOpeningHours {
			weekDay 
			hours { 
				from 
				to 
			}
		}'''
		val model = parseHelper.parse(code)
		validationHelper.assertNoIssues(model)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.files.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "default/WeekdayOpeningHoursCloner.java"
		assertTrue(fsa.files.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package default;
			public class WeekdayOpeningHoursCloner {
				public a.b.c.WeekdayOpeningHours apply(a.b.c.WeekdayOpeningHours other) {
					a.b.c.WeekdayOpeningHours it = new a.b.c.WeekdayOpeningHours();
					it.setWeekDay(other.getWeekDay());
					it.setHours(hoursCloner.apply(other.getHours()));
					return it;
				}
				
				private final HoursCloner hoursCloner = new HoursCloner();
				public static class HoursCloner {
					public a.b.c.FromToItem apply(a.b.c.FromToItem other) {
						a.b.c.FromToItem it = new a.b.c.FromToItem();
						it.setFrom(other.getFrom());
						it.setTo(other.getTo());
						return it;
					}
				}
			}
			'''.toString, 
			fsa.files.get(topClonerJavaFileName).toString
		)
	}
	
	
	@Test
	def testDeepStructure() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		a.b.m.Magazine {
			name
			isbn
			article {
				name
				author
				pageNumber
				section {
					name
					description
					paragraph {
						number
						text
					}
				}
			}
		}
		'''
		val model = parseHelper.parse(code)
		validationHelper.assertNoIssues(model)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.files.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "my/home/cloners/MagazineCloner.java"
		assertTrue(fsa.files.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package my.home.cloners;
			public class MagazineCloner {
				public a.b.m.Magazine apply(a.b.m.Magazine other) {
					a.b.m.Magazine it = new a.b.m.Magazine();
					it.setName(other.getName());
					it.setIsbn(other.getIsbn());
					it.setArticle(articleCloner.apply(other.getArticle()));
					return it;
				}
				
				private final ArticleCloner articleCloner = new ArticleCloner();
				public static class ArticleCloner {
					public a.b.m.Article apply(a.b.m.Article other) {
						a.b.m.Article it = new a.b.m.Article();
						it.setName(other.getName());
						it.setAuthor(other.getAuthor());
						it.setPageNumber(other.getPageNumber());
						it.setSection(sectionCloner.apply(other.getSection()));
						return it;
					}
					
					private final SectionCloner sectionCloner = new SectionCloner();
					public static class SectionCloner {
						public a.b.m.Section apply(a.b.m.Section other) {
							a.b.m.Section it = new a.b.m.Section();
							it.setName(other.getName());
							it.setDescription(other.getDescription());
							it.setParagraph(paragraphCloner.apply(other.getParagraph()));
							return it;
						}
						
						private final ParagraphCloner paragraphCloner = new ParagraphCloner();
						public static class ParagraphCloner {
							public a.b.m.Paragraph apply(a.b.m.Paragraph other) {
								a.b.m.Paragraph it = new a.b.m.Paragraph();
								it.setNumber(other.getNumber());
								it.setText(other.getText());
								return it;
							}
						}
					}
				}
			}
			'''.toString, 
			fsa.files.get(topClonerJavaFileName).toString
		)
	}
	
	@Test
	def testDeepStructureWithExclusions() {
		val code = '''
		deepClone
		
		package my.home.cloners.reduced
		
		a.b.m.Magazine {
			name
			-isbn
			article {
				name
				-author
				-pageNumber
				section {
					-name
					description
					-paragraph
				}
			}
		}
		'''
		val model = parseHelper.parse(code)
		validationHelper.assertNoIssues(model)
		val fsa = new InMemoryFileSystemAccess()
		generatorDslToJava.doGenerate(model.eResource, fsa)
		assertEquals(1, fsa.files.size)
		val topClonerJavaFileName = IFileSystemAccess::DEFAULT_OUTPUT + "my/home/cloners/reduced/MagazineCloner.java"
		assertTrue(fsa.files.containsKey(topClonerJavaFileName))
		assertEquals(
			'''
			package my.home.cloners.reduced;
			public class MagazineCloner {
				public a.b.m.Magazine apply(a.b.m.Magazine other) {
					a.b.m.Magazine it = new a.b.m.Magazine();
					it.setName(other.getName());
					it.setArticle(articleCloner.apply(other.getArticle()));
					return it;
				}
				
				private final ArticleCloner articleCloner = new ArticleCloner();
				public static class ArticleCloner {
					public a.b.m.Article apply(a.b.m.Article other) {
						a.b.m.Article it = new a.b.m.Article();
						it.setName(other.getName());
						it.setSection(sectionCloner.apply(other.getSection()));
						return it;
					}
					
					private final SectionCloner sectionCloner = new SectionCloner();
					public static class SectionCloner {
						public a.b.m.Section apply(a.b.m.Section other) {
							a.b.m.Section it = new a.b.m.Section();
							it.setDescription(other.getDescription());
							return it;
						}
					}
				}
			}
			'''.toString, 
			fsa.files.get(topClonerJavaFileName).toString
		)
	}
}
