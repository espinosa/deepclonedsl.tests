package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.Model
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.eclipse.xtext.serializer.ISerializer
import org.eclipse.xtext.resource.SaveOptions

import static org.junit.Assert.*

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneFormatterTest {
	
	@Inject ParseHelper<Model> parser
	@Inject ValidationTestHelper validationHelper
	@Inject extension ISerializer
	
	@Test
	def void testSerializationWithNoFormatting() {
		val sample = '''
		deepClone
		package a.b.m    
		a.b.m.Paragraph {
			number
			text    
		}
		'''
		val expectedFormattedSample = '''
		deepClone
		package a.b.m    
		a.b.m.Paragraph {
			number
			text    
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
 
	@Test
	def void formatSample1() {
		val sample = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
		number
		text
		}
		'''
		val expectedFormattedSample = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			number
			text
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize(
				SaveOptions::newBuilder.format().getOptions() // if this is omitted, then there is no formatting
			)
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
	
	@Test
	def void validateCorrectDeepModelWithComments() {
		val sample = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			name  // end of   line comment
			article {
				name
				-author
				-pageNumber
				section {
					name 
					description 
					-paragraph
				}
			}
		}
		'''
		
		val expectedFormattedSample = '''
		deepClone
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			name // end of   line comment
			article {
				name
				-author
				-pageNumber
				section {
					name
					description
					-paragraph
				}
			}
		}'''.toString
		val model = parser.parse(sample)
		validationHelper.assertNoErrors(model)
		val resultFormattedCode = model.serialize(
				SaveOptions::newBuilder.format().getOptions() // if this is omitted, then there is no formatting
			)
		assertEquals(
			expectedFormattedSample,
			resultFormattedCode
		)
	}
}