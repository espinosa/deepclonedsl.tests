package my.home.dsl.deepclone

import com.google.inject.Inject
import my.home.dsl.DeepCloneInjectorProvider
import my.home.dsl.deepClone.DeepClonePackage
import my.home.dsl.deepClone.DeepClonePackage$Literals
import my.home.dsl.deepClone.Model
import my.home.dsl.deepclone.test.utils.MyValidationHelper
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.eclipse.xtext.junit4.validation.ValidationTestHelper
import org.junit.Test
import org.junit.runner.RunWith

import static org.junit.Assert.*
import my.home.dsl.validation.DeepCloneJavaValidator

@InjectWith(typeof(DeepCloneInjectorProvider))
@RunWith(typeof(XtextRunner))
class DeepCloneValidationTest {
	
	@Inject ParseHelper<Model> parser
	@Inject ValidationTestHelper validationHelper
	@Inject MyValidationHelper myValidationHelper
 
	@Test
	def void validateCorrectModelWithInclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			number
			text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void validateCorrectModelWithExclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			-number
			-text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	@Test
	def void validateCorrectModelWithInclusionsAndExclusions() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			-number
			text
		}
		'''
		val model = parser.parse(code)
		validationHelper.assertNoErrors(model)
	}
	
	/** missing 'number' and surplus 'fooField' fields 
	 *  official approach to assert results 
	 */
	def void validateIncorrectModel_MissingAndSurplus() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			text
			fooField
		}
		'''
		val model = parser.parse(code)
		val missingFieldErrorEClass = DeepClonePackage$Literals::CLASS_CLONER  // parent element has to be provided, not   
		// Note:  DeepClonePackage::eINSTANCE.classCloner == DeepClonePackage$Literals::CLASS_CLONER
		val surplusFieldErrorEClass = DeepClonePackage$Literals::FIELD_CLONER_TYPE
		validationHelper.assertError(model, missingFieldErrorEClass, DeepCloneJavaValidator::MISSING_FIELDS_ERROR, "Field number missing declaration")
		validationHelper.assertError(model, surplusFieldErrorEClass, DeepCloneJavaValidator::SURPLUS_FIELDS_ERROR, "Field fooField is not defined in the class a.b.m.Paragraph")
	}
	
	/** missing 'number' and surplus 'fooField' fields; different approach to assert results */
	@Test
	def void validateIncorrectModel_MissingAndSurplus2() {
		val code = '''
		deepClone
		package a.b.m
		a.b.m.Paragraph {
			text
			fooField
		}
		'''
		val model = parser.parse(code)
		val s1 = '''
			Field number missing declaration, line: 3, object: ClassCloner
			a.b.m.Paragraph has missing declaration, line: 3, object: ClassCloner
			Field fooField is not defined in the class a.b.m.Paragraph, line: 5, object: SimpleField
			a.b.m.Paragraph has surplus declarations, line: 3, object: ClassCloner
			'''.toString
		val s2 = myValidationHelper.validate(model).issuesToString 
		assertEquals(
			s1, s2
		)
	}
	
	@Test
	def void validateIncorrectDeepModel_MissingAndSurplus() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		a.b.m.Magazine {
			-isbn
			article {
				name
				-author
				-pageNumber
				section {
					-name
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		println(validationHelper.validate(model));
		assertEquals(
			'''
			Field name missing declaration, line: 5, object: ClassCloner
			a.b.m.Magazine has missing declaration, line: 5, object: ClassCloner
			Field description missing declaration, line: 11, object: ComplexField
			section has missing declaration, line: 11, object: ComplexField
			'''.toString.trim,
			 myValidationHelper.validate(model).issuesToString.trim
		)
	}
	
	@Test
	def void validateIncorrectDeepModelWithComments() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			// declaration
			article {
				name
				-author
				-pageNumber
				section {
					-name
					/*
					  description
					 */
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		println(validationHelper.validate(model));
		assertEquals(
			'''
			Field name missing declaration, line: 7, object: ClassCloner
			a.b.m.Magazine has missing declaration, line: 7, object: ClassCloner
			Field description missing declaration, line: 14, object: ComplexField
			section has missing declaration, line: 14, object: ComplexField
			'''.toString.trim,
			 myValidationHelper.validate(model).issuesToString.trim
		)
	}
	
	@Test
	def void validateCorrectDeepModelWithComments() {
		val code = '''
		deepClone
		
		package my.home.cloners
		
		/** javadoc style comment */
		//some cooment
		a.b.m.Magazine {
			-isbn
			name  // end of line comment
			article {
				name
				-author
				-pageNumber
				section {
					name 
					description 
					-paragraph
				}
			}
		}
		'''
		val model = parser.parse(code)
		println(validationHelper.validate(model));
		assertEquals(
			"",
			 myValidationHelper.validate(model).issuesToString.trim
		)
	}
}