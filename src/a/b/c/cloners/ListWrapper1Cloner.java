package a.b.c.cloners;
public class ListWrapper1Cloner {
	public a.b.c.ListWrapper1 apply(a.b.c.ListWrapper1 other) {
		a.b.c.ListWrapper1 it = new a.b.c.ListWrapper1();
		it.setPersonNames(personNamesCollectionCloner(it.getPersonNames(), other.getPersonNames()));
		return it;
	}

	private java.util.List<java.lang.String> personNamesCollectionCloner(java.util.List<java.lang.String> thisCollection, java.util.List<java.lang.String> otherCollection) {
		for (java.lang.String otherCollectionItem : otherCollection) {
			thisCollection.add(otherCollectionItem);
		}
		return thisCollection;
	}
}
