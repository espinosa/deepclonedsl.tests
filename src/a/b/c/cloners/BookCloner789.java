package a.b.c.cloners;
public class BookCloner789 {
	public a.b.m2.Book apply(a.b.m2.Book other) {
		a.b.m2.Book it = new a.b.m2.Book();
		it.setName(other.getName());
		it.setAuthor(other.getAuthor());
		it.setSections(sectionsCollectionCloner(it.getSections(), other.getSections()));
		return it;
	}
	
	private java.util.List<a.b.m2.Section> sectionsCollectionCloner(java.util.List<a.b.m2.Section> thisCollection, java.util.List<a.b.m2.Section> otherCollection) {
		for (a.b.m2.Section otherCollectionItem : otherCollection) {
			thisCollection.add(sectionsCloner.apply(otherCollectionItem));
		}
		return thisCollection;
	}
	
	private final SectionsCloner sectionsCloner = new SectionsCloner();
	public static class SectionsCloner {
		public a.b.m2.Section apply(a.b.m2.Section other) {
			a.b.m2.Section it = new a.b.m2.Section();
			it.setName(other.getName());
			it.setDescription(other.getDescription());
			return it;
		}
	}
}