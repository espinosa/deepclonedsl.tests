package a.b.c;

import java.util.List;

public class ListWrapper1 {
	List<String> personNames;

	public List<String> getPersonNames() {
		return personNames;
	}

	public void setPersonNames(List<String> personNames) {
		this.personNames = personNames;
	}
}
