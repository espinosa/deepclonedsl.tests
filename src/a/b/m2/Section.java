package a.b.m2;

import java.util.Set;

/** test dummy class */
public class Section {
	private String name; 
	private String description; 
	private Set<Paragraph> paragraphs;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<Paragraph> getParagraphs() {
		return paragraphs;
	}
	public void setParagraphs(Set<Paragraph> paragraphs) {
		this.paragraphs = paragraphs;
	}
}
